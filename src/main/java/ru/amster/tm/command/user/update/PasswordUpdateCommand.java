package ru.amster.tm.command.user.update;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class PasswordUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "upd-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - updating user password";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER PASSWORD");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updatePassword(userId, password);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}