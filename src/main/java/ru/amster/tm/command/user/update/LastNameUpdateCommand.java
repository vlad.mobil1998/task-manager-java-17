package ru.amster.tm.command.user.update;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class LastNameUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "upd-last-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - updating user last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE LAST NAME]");
        System.out.println("ENTER LAST NAME");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String lastName = TerminalUtil.nextLine();
        if (lastName == null || lastName.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updateLastName(userId, lastName);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}