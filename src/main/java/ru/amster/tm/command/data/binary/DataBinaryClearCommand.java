package ru.amster.tm.command.data.binary;

import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.enamuration.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove binary data";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BINARY CLEAR]");
        final File file = new File(FILE_BINARY);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}