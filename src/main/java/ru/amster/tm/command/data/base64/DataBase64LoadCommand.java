package ru.amster.tm.command.data.base64;

import ru.amster.tm.command.data.AbstractDataCommand;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.enamuration.Role;
import sun.misc.BASE64Decoder;

import java.io.*;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Load base64 data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 LOAD]");

        File file = new File(FILE_BASE64);
        final FileInputStream fileInputStream = new FileInputStream(file);

        byte[] base64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        fileInputStream.close();
        objectInputStream.close();
        byteArrayInputStream.close();

        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}