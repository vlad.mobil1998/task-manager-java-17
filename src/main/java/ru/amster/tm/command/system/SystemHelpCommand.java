package ru.amster.tm.command.system;

import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class SystemHelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return " - Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        ITerminalService terminalService = new TerminalService();
        final Collection<AbstractCommand> commands = terminalService.getCommands();
        for (AbstractCommand command : commands) {
            if (command.arg() == null) System.out.println(command.name() + command.description());
            else System.out.println(command.name() + " " + command.arg() + command.description());
        }
    }

}