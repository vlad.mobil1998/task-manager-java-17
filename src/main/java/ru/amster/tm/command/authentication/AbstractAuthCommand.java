package ru.amster.tm.command.authentication;

import ru.amster.tm.command.AbstractCommand;

public abstract class AbstractAuthCommand extends AbstractCommand {

    protected UserActivated userActivated;

    public void setLoginCheck(UserActivated userActivated) {
        this.userActivated = userActivated;
    }

}