package ru.amster.tm.command.authentication;

import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.user.AccessDeniedException;

public class LogoutCommand extends AbstractAuthCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Log out system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (!userActivated.getCheckForActivation()) throw new AccessDeniedException();
        serviceLocator.getAuthService().logout();
        userActivated.setCheckForActivation(false);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}