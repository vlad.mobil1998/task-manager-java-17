package ru.amster.tm.service;

import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.util.HashUtil;

public class AuthenticationService implements IAuthenticationService {

    private final IUserService userService;

    private String userId = null;

    public AuthenticationService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registration(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

}