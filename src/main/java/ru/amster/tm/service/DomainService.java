package ru.amster.tm.service;

import ru.amster.tm.api.servise.*;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.exception.empty.EmptyDomainException;

public class DomainService implements IDomainService{

    IProjectService projectService;

    ITaskService taskService;

    IUserService userService;

    public DomainService(
            IProjectService projectService,
            ITaskService taskService,
            IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        domain.setUsers(userService.export());
        domain.setProjects(projectService.export());
        domain.setTasks(taskService.export());
    }

}