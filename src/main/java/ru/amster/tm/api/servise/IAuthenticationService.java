package ru.amster.tm.api.servise;

import ru.amster.tm.enamuration.Role;

public interface IAuthenticationService {

    String getUserId();

    void login(String login, String password);

    void logout();

    void registration(String login, String password, String email);

    void checkRoles(Role[] roles);

}