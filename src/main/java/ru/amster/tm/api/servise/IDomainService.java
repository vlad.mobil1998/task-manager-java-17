package ru.amster.tm.api.servise;

import ru.amster.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void  export(Domain domain);

}