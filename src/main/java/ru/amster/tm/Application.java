package ru.amster.tm;

import ru.amster.tm.bootstrap.Bootstrap;

import java.io.IOException;

public final class Application {

    public static void main(final String[] args) throws IOException, ClassNotFoundException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}