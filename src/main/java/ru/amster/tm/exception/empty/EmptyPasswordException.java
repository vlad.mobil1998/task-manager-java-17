package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("ERROR! Password is empty...");
    }

}