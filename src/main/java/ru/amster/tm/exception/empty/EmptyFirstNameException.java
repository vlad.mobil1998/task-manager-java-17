package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}